
package com.info.assigments.assigment1;

import java.util.Scanner;

public class ArthmeticOpUsingNinjaLang {

    final int replaceDigit=7;
	 boolean isDigitPresent(int number, int digit) {

		while (number > 0) {
			if (number % 10 == digit)
				break;
			number = number / 10;
		}
		return (number > 0);
	}

	 int replaceDigit(int arthmeticOp, int digit, int replaceDigit) {
		int result = 0, multiply = 1;

		while (arthmeticOp % 10 > 0) {

			int remainder = arthmeticOp % 10;

			if (remainder == digit)
				result = result + replaceDigit * multiply;

			else
				result = result + remainder * multiply;
			multiply *= 10;
			arthmeticOp = arthmeticOp / 10;
		}
		return result;
	}
	 
	public void checkOperation(int result, int digit, int number1, int number2)
	{
		if (result == digit || isDigitPresent(result, digit)) {
			System.out.println("result contains digit 8");
			int resultAfterSkip = replaceDigit(result, digit, replaceDigit);
			System.out.println("so as ninja's language result is :"+ resultAfterSkip);
		} else
			System.out.println("result: " + result);	
	}
	
	 void arthmeticOperation(String operation, int digit) {
		Scanner firstNumber = new Scanner(System.in);
		System.out.println("Enter 1st number");
		int number1 = firstNumber.nextInt();

		Scanner secondNumber = new Scanner(System.in);
		System.out.println("Enter 2nd number");
		int number2 = secondNumber.nextInt();

		switch (operation) {
		case "addition":
			int resultOfAddition = number1 + number2;
			checkOperation(resultOfAddition, digit, number1, number2);
			break;
		case "subsraction":
			int resultOfSubtraction = number1 - number2;
			checkOperation(resultOfSubtraction, digit, number1, number2);
			break;
		case "multiplication":
			int resultOfMultiplication = number1 * number2;
			checkOperation(resultOfMultiplication, digit, number1, number2);
			break;
		case "division":
			int resultOfDivision = number1 / number2;
			checkOperation(resultOfDivision, digit, number1, number2);
			break;
		default:
			break;
		}

	}
	public static void main(String[] args) {
		ArthmeticOpUsingNinjaLang classObject=new ArthmeticOpUsingNinjaLang();
		Scanner inputType = new Scanner(System.in);
		System.out.println("Enter input type :");
		String operation = inputType.next();
		Scanner skipNumber = new Scanner(System.in);
		System.out.println("Enter digit which you want to delete");
		int digit = skipNumber.nextInt();
		classObject.arthmeticOperation(operation, digit);
	}

}
