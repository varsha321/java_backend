package com.info.assigments.assigment1;
import java.util.Scanner;


public class TableUsingNinjaLang {

	boolean isDigitPresent(int tillNumber, int digit) {

		while (tillNumber > 0) {
			if (tillNumber % 10 == digit)
				break;
			tillNumber = tillNumber / 10;
		}
		return (tillNumber > 0);
	}

	 void printNumbers(int tillNumber, int digit) {
		for (int i = 0; i <= tillNumber; i++)
			if (i == digit || isDigitPresent(i, digit))
				System.out.print("\n");
			else {
				System.out.println(i + " ");
			}
	}

	public static void main(String[] args) {
		TableUsingNinjaLang classObject=new TableUsingNinjaLang();
		Scanner endnumber = new Scanner(System.in);
		System.out.println("Enter number");
		int tillNumber = endnumber.nextInt();

		Scanner skipNumber = new Scanner(System.in);
		System.out.println("Enter digit which you want to delete");
		int digit = skipNumber.nextInt();

		classObject.printNumbers(tillNumber, digit);
	}
}
